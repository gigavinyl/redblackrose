(set-env!
 :source-paths    #{"src"}
 :resource-paths  #{"resources"}
 :dependencies '[[org.clojure/clojure "1.10.1"]])

(def version "0.1.0-SNAPSHOT")
(task-options! pom {:project 'redblackrose
                    :version (str version "-standalone")
                    :description "FIXME: write description"
                    :license {"Eclipse Public License" "http://www.eclipse.org/legal/epl-v10.html"}})

;; == Cider Support =======================================

(require 'boot.repl)
(swap! boot.repl/*default-dependencies*
       concat '[[cider/cider-nrepl "0.10.0-snapshot"]])

(swap! boot.repl/*default-middleware*
       conj 'cider.nrepl/cider-middleware)

;; -- My Tasks --------------------------------------------
